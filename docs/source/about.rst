About plotID
==================================

plotID is developed at the `Chair of Fluid Systems (FST)` <https://www.fst.tu-darmstadt.de>`_, TU Darmstadt within the project `NFDI4Ing <https://www.nfdi4ing.de>`_.

Acknowledgements
----------------

The authors would like to thank the Federal Government and the Heads of Government of the Länder, as well as the Joint Science Conference (GWK), for their funding and support within the framework of the NFDI4Ing consortium. Funded by the German Research Foundation (DFG) - project number `442146713 <https://gepris.dfg.de/gepris/projekt/442146713?context=projekt&task=showDetail&id=442146713&>`_.
