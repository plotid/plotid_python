Dependencies of plotID
==================================

The plotID package depends on other software for its full funcionality. Since plotID is a Python program, Python ≥ 3.10 is needed. A list of all dependencies and their connection to plotID is provided here.

- `matplotlib <https://matplotlib.org/stable/index.html>`_: Famous plotting engine. matplotlib plots and figures can be tagged and published with plotID.
- `Pillow <https://pillow.readthedocs.io/en/stable/>`_: Library for image processing. If no plotting engine was used that is supported by plotID, image files can be tagged as well. This is done with Pillow. Additionally, Pillow is used to print the QR codes on the plots.
- `qrcode <https://github.com/lincolnloop/python-qrcode>`_: Creates QR codes from input data. qrcode is used to create the QR code which is printed on the plot in the end.
- `numpy <https://numpy.org/doc/stable/>`_: Package for scientific computing. Right now it is only used to generate sample data in the examples. Since it is a dependency of matplotlib, it has to be installed anyway.


Optional dependencies that are not necessary to run plotID but more to contribute to the code quality are the following:

- `coverage <https://coverage.readthedocs.io/en/6.5.0/#>`_: Program to measure the code coverage. It is used in the development of plotID to monitor the coverage of the code when the unit tests are run.
- `sphinx <https://www.sphinx-doc.org/en/master/>`_: Program to build documentation. For example, this documentatio is generated with the help of sphinx.
- `sphinx-autoapi <https://sphinx-autoapi.readthedocs.io/en/latest/>`_: Generates documentation files automatically from documentation inside the code. This has the advantage that code can be documented inside the code but can also be read in the documentation without having two places with the same content.
- `sphinx-rtd-theme <https://sphinx-rtd-theme.readthedocs.io/en/stable/>`_: Readthedocs theme for nicer documentation style.
- `myst-parser <https://myst-parser.readthedocs.io/en/latest/>`_: Extension for sphinx, e.g. to include markdown files in the documentation. That allows to include plotID's README.md in the documentation without storing it at two different places.
