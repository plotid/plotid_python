Customization of plotID
=======================
How your plot will be tagged by plotID is highly customizable. Please have a look in the documentation of the function "tagplot" to get an overview of all available keyword arguments.

To use a custom font for the ID on your plot, it is necessary to specify the absolute path to the ".otf" or ".ttf" file. You can put the file in your working directory and enter its path as argument in the script which calls plotID. You can also use fonts installed on your system. Below is a list of default locations of system wide installed fonts.

Linux:

- /usr/share/fonts
- /usr/local/share/fonts
- ~/.fonts

Windows:

- C:\\Windows\\Fonts

MacOS:

- /Library/Fonts/
