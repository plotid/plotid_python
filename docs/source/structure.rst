Structure and architecture of plotID
====================================

A visualization of the internal structure of plotID is provided here. The structure tries to follow Object Oriented Programming (OOP) principles. First tagplot() is depicted with matplotlib as example plot engine:

|structure tagplot|

In the following is shown how a call to publish is processed by plotID:

|structure publish|




.. |structure tagplot| image:: _static/tagplot.png
  :width: 700
  :alt: Graph to show the architecture of the function tagplot().

.. |structure publish| image:: _static/publish.png
  :width: 700
  :alt: Graph to show the architecture of the function publish().


