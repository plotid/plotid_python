.. plotID documentation master file, created by
   sphinx-quickstart on Tue Jun 21 14:09:27 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to plotID's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

    Overview <readme_link.rst>
    Installation <installation.rst>
    Customization <customization.rst>
    Dependencies <dependencies.rst>
    Structure and architecture <structure.rst>
    About <about.rst>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
