Installation of plotID
==================================

For installation instructions, first have a look at the general :doc:`readme_link` or the `README.md <https://git.rwth-aachen.de/plotid/plotid_python/-/blob/main/README.md>`_ file. More detailed information about the installation process can then be found here.

Wrong python version
--------------------
To install and run plotID a python version ≥ 3.10 is required. If your linux distribution is shipped with a python version < 3.10, you still can install a newer python version. Intalling python 3.10 on Debian or Ubuntu would look like this

.. code:: bash

    sudo apt install python3.10 python3.10-venv

You can then create a virtual environment "venv" to run plotID inside it

.. code:: bash

    python3.10 -m venv venv

Now activate the venv

.. code:: bash

    source venv/bin/activate

The virtual environment now runs with python 3.10 and plotID can be installed the usual way as described in :doc:`Overview`.


Installation on Windows via pip
-------------------------------
If you're trying

.. code:: cmd

    pip install .

and following error is thrown

.. code:: cmd

    Fatal error in launcher: Unable to create process using '"...env\Scripts\python.exe"  "..\venv\Scripts\pip.exe" install .': The system cannot find the specified file.

Try to forcefully reinstall pip

.. code:: cmd

    python -m pip install --upgrade --force pip

Or use 

.. code:: cmd

    python -m pip install .

instead.
