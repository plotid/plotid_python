"""
plotID for Python.

This is the python plotID project.
plotID is a program connected to Research Data Management (RDM).
It has two main functionalities:
1. Tag your plot with an identifier.
2. Export the resulting file to a specified directory along the corresponding
research data, the plot is based on. Additionally, the script that created the
plot will also be copied to the directory.
"""

__version__ = "0.3.2"
__author__ = "Institut Fluidsystemtechnik within nfdi4ing at TU Darmstadt"
